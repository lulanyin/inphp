<?php
// +----------------------------------------------------------------------
// | INPHP
// +----------------------------------------------------------------------
// | Copyright (c) 2021 https://inphp.cc All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( https://opensource.org/licenses/MIT )
// +----------------------------------------------------------------------
// | Author: lulanyin <me@lanyin.lu>
// +----------------------------------------------------------------------
namespace Inphp;

use Inphp\Util\Arr;
use Inphp\Util\File;

class Config
{
    /**
     * 所有配置
     * @var array
     */
    public static array $configs = [];

    /**
     * 加载配置
     * @param string $path 特定路径
     * @return array
     */
    public static function loadConfig(string $path) : array
    {
        $files = File::getFiles(self::get("define.configs")."/{$path}", "php", null);
        if(!empty($files)){
            $configs = [];
            foreach ($files as $file){
                $config = include $file['path'];
                //去看后尾 .php 4个字符，得到文件名
                $name = substr($file['filename'], 0, -4);
                $configs[$name] = $config;
            }
            return self::set($path, Arr::parseValue($configs, self::$configs));
        }
        return [];
    }

    /**
     * 更改配置，并保存
     * @param string $path
     * @param array $config
     * @return bool
     */
    public static function updateConfigFile(string $path, array $config = []): bool
    {
        $old_config = self::get($path);
        $old_config = is_array($old_config) ? $old_config : [];
        $new_config = array_merge($old_config, $config);
        $path = str_replace(".", "/", $path);
        if($f = fopen(self::get("define.configs")."/".$path.".php", "w"))
        {
            fwrite($f, "<?php\r\n//最后更新于 ".date("Y/m/d H:i:s")."\r\nreturn ".var_export($new_config, true).";");
            fclose($f);
            return true;
        }
        return false;
    }

    /**
     * 获取配置
     * @param string|null $path
     * @param mixed $default
     * @return mixed
     */
    public static function get(string|null $path = null, mixed $default = null) : mixed
    {
        if(is_null($path)){
            return self::$configs;
        }else{
            $path = str_replace("/", ".", $path);
            $pathArr = explode(".", $path);
            if(!isset(self::$configs[$pathArr[0]])){
                if($values = self::loadConfig($pathArr[0])){
                    return count($pathArr)>1 ? Arr::get($values, join(".", array_slice($pathArr, 1)), $default) : $values;
                }else{
                    return $default;
                }
            }else{
                return Arr::get(self::$configs, $path, $default);
            }
        }
    }

    /**
     * 保存配置
     * @param string $name
     * @param array $values
     * @return array
     */
    public static function set(string $name, array $values) : array
    {
        if(isset(self::$configs[$name])){
            unset(self::$configs[$name]);
        }
        self::$configs[$name] = $values;
        return $values;
    }
}