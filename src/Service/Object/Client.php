<?php
// +----------------------------------------------------------------------
// | INPHP
// +----------------------------------------------------------------------
// | Copyright (c) 2021 https://inphp.cc All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( https://opensource.org/licenses/MIT )
// +----------------------------------------------------------------------
// | Author: lulanyin <me@lanyin.lu>
// +----------------------------------------------------------------------
namespace Inphp\Service\Object;

use Inphp\Config;
use Inphp\Service\Context;

/**
 * 客户端数据对象
 * Class Client
 * @package Inphp\Service\Object
 */
class Client
{
    /**
     * 客户端ID
     * 可以使用 swoole request 对象的 fd
     * 也可以自定义
     * @var int
     */
    public int $id = -1;

    /**
     * PHP_SESSION_ID
     * 若使用的是swoole服务，则需要另行实现
     * @var string|null
     */
    public string|null $php_session_id = null;

    /**
     * 请求域名
     * @var string
     */
    public string $host = '127.0.0.1';

    /**
     * 是不是用https访问
     * @var bool
     */
    public bool $https = false;

    /**
     * 来源域名
     * @var string
     */
    public string $origin = '127.0.0.1';

    /**
     * 请求方式
     * @var string
     */
    public string $method = "GET";
    
    /**
     * $_SERVER
     * @var array
     */
    public array $server = [];

    /**
     * cookie
     * @var array
     */
    public array $cookies = [];

    /**
     * session
     * @var array
     */
    public array $sessions = [];

    /**
     * 地址参数
     * @var array
     */
    public array $get = [];

    /**
     * post参数
     * @var array
     */
    public array $post = [];

    /**
     * GET和POST的参数集
     * @var array
     */
    public array $request = [];

    /**
     * HTTP_RAW_POST_DATA
     * @var string
     */
    public string $raw_post_data = '';

    /**
     * 文件上传
     * @var array
     */
    public array $files = [];

    /**
     * 客户端IP
     * @var string
     */
    public string $ip = '127.0.0.1';

    /**
     * 请求路径
     * @var string
     */
    public string $uri = '';

    /**
     * 是否是AJAX请求
     * @var bool
     */
    public bool $ajax = false;

    /**
     * Client constructor.
     * @param array $values
     */
    public function __construct(array $values = [])
    {
        $this->host     = $values['host'] ?? '127.0.0.1';
        $this->origin   = $values['origin'] ?? '127.0.0.1';
        $this->method   = $values['method'] ?? 'get';
        $this->method   = strtoupper($this->method);
        $this->ip       = $values['ip'] ?? '127.0.0.1';
        $this->get      = $values['get'] ?? [];
        $this->post     = $values['post'] ?? [];
        $this->files    = $values['files'] ?? [];
        $this->cookies  = $values['cookie'] ?? [];
        $this->sessions = $values['session'] ?? [];
        $this->raw_post_data  = $values['raw_post_data'] ?? '';
        $this->uri      = $values['uri'] ?? '';
        $this->ajax     = $values['ajax'] ?? false;
        $this->https    = $values['https'] ?? false;
        $this->id       = $values['id'] ?? -1;
        $this->php_session_id = $values['php_session_id'] ?? null;
        $this->request = array_merge($this->get, $this->post);
        //2022.5.4 增加
        $this->server   = $values['server'] ?? [];
    }

    /**
     * 获取属性值
     * @param $name
     * @return mixed
     */
    public function get($name) : mixed {
        return property_exists($this, $name) ? $this->{$name} : null;
    }

    /**
     * 设置值
     * @param $name
     * @param $value
     */
    public function set($name, $value) : void {
        $this->{$name} = $value;
    }

    /**
     * 获取cookie值
     * @param string|null $name
     * @return string|null
     */
    public function getCookie(string|null $name = null) : string|null
    {
        if(is_null($name)){
            return $this->cookies;
        }
        if(!isset($this->cookies[$name]) || !isset($this->cookies[$name."_hash"])){
            return null;
        }
        $value = $this->cookies[$name];
        $hash = $this->cookies[$name."_hash"];
        $config = Config::get('service.http');
        $key = $config['cookie']['hash_key'] ?? '123456';
        if(hash_hmac("sha1", $value, $key) == $hash){
            return $value;
        }
        return null;
    }

    /**
     * 保存Cookie
     * @param string $name
     * @param string|null $value
     */
    public function setCookie(string $name, string|null $value = null, int $time = 86400) : void
    {
        $response = Context::getResponse();
        $response->cookie($name, $value, $time);
    }

    /**
     * 删除cookie
     * @param string|null $name
     */
    public function dropCookie(string|null $name = null) : void
    {
        $this->setCookie($name, null);
    }

    /**
     * 获取token
     * @return string|null
     */
    public function getToken() : string|null
    {
        return $this->get['token'] ?? ($this->post['token'] ?? $this->getCookie('token'));
    }
}