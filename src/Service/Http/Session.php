<?php
// +----------------------------------------------------------------------
// | INPHP
// +----------------------------------------------------------------------
// | Copyright (c) 2021 https://inphp.cc All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( https://opensource.org/licenses/MIT )
// +----------------------------------------------------------------------
// | Author: lulanyin <me@lanyin.lu>
// +----------------------------------------------------------------------
namespace Inphp\Service\Http;

use Inphp\Cache;
use Inphp\Service\Context;
use Inphp\Service\Service;

/**
 * HTTP SESSION
 * Class Session
 * @package Inphp\Service\Http
 */
class Session
{

    /**
     * 获取 session
     * @param string|null $name
     * @param null $default
     * @return mixed
     */
    public static function get(string $name = null, $default = null) : mixed
    {
        if(Service::isSwoole()) {
            $client = Context::getClient();
            $php_session_id = $client->getCookie("PHP_SESSION_ID");
            if(!empty($php_session_id)){
                $key_name = self::getKeyName($php_session_id);
                //从缓存中取
                $data = Cache::get($key_name, $default);
                $data = is_array($data) ? $data : [];
                if(is_null($name)){
                    $list = [];
                    foreach ($data as $key => $d){
                        $list[$key] = $d['value'];
                    }
                    return $list;
                }
                $value = $data[$name] ?? [];
                return $value['value'] ?? $default;
            }
            return $default;
        }else{
            if(session_status() !== PHP_SESSION_ACTIVE){
                @session_start();
            }
            return !empty($name) ? ($_SESSION[$name] ?? $default) : $_SESSION;
        }
    }


    /**
     * 保存 session
     * @param string $name
     * @param mixed $value
     */
    public static function set(string $name, mixed $value){
        if(Service::isSwoole()){
            $client = Context::getClient();
            $php_session_id = $client->getCookie("PHP_SESSION_ID");
            if(empty($php_session_id)){
                $php_session_id = sha1(microtime(true).rand(0,999999));
                $client->setCookie("PHP_SESSION_ID", $php_session_id);
            }
            $key_name = self::getKeyName($php_session_id);
            //从缓存中取
            $data = Cache::get($key_name);
            $data = is_array($data) ? $data : [];
            $data[$name] = [
                "value" => $value,
                "time"  => time()
            ];
            Cache::set($key_name, $data);
        }else{
            if(session_status() !== PHP_SESSION_ACTIVE){
                @session_start();
            }
            $_SESSION[$name] = $value;
        }
    }

    /**
     * 移除 session
     * @param string|null $name
     */
    public static function remove(string|null $name = null){
        if(Service::isSwoole()) {
            $client = Context::getClient();
            $php_session_id = $client->getCookie("PHP_SESSION_ID");
            if(!empty($php_session_id)){
                $key_name = self::getKeyName($php_session_id);
                //从缓存中取
                $data = Cache::get($key_name);
                $data = is_array($data) ? $data : [];
                if(is_null($name)){
                    Cache::remove($key_name);
                }elseif(isset($data[$name])){
                    unset($data[$name]);
                    if(!empty($data)){
                        Cache::set($key_name, $data);
                    }else{
                        Cache::remove($key_name);
                    }
                }
            }
        }else{
            if(session_status() !== PHP_SESSION_ACTIVE){
                @session_start();
            }
            if(is_null($name)){
                $_SESSION = [];
            }else{
                if(isset($_SESSION[$name])) unset($_SESSION[$name]);
            }
        }
    }

    /**
     * 处理缓存名称
     * @param string $name
     * @return string
     */
    private static function getKeyName(string $name): string
    {
        return "session_".$name;
    }
}