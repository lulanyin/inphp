<?php
// +----------------------------------------------------------------------
// | INPHP
// +----------------------------------------------------------------------
// | Copyright (c) 2021 https://inphp.cc All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( https://opensource.org/licenses/MIT )
// +----------------------------------------------------------------------
// | Author: lulanyin <me@lanyin.lu>
// +----------------------------------------------------------------------
namespace Inphp\Helper;

class SmartyHelper
{
    /**
     * 所有插件
     * @var array[]
     */
    public static $pliguns = [
        "function"  => [],
        "block"     => [],
        "modifier"  => []
    ];

    /**
     * @param string $name
     * @param mixed $fn
     * @return void
     */
    public static function registerPluginFunction(string $name, mixed $fn): void
    {
        self::$pliguns["function"][$name] = $fn;
    }

    /**
     * @param string $name
     * @param mixed $fn
     * @return void
     */
    public static function registerPluginBlock(string $name, mixed $fn): void
    {
        self::$pliguns["block"][$name] = $fn;
    }

    /**
     * @param string $name
     * @param mixed $fn
     * @return void
     */
    public static function registerPluginModifier(string $name, mixed $fn): void
    {
        self::$pliguns["modifier"][$name] = $fn;
    }

}